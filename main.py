from form import Ui_gustav_beck_build_calculator

from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc
import webbrowser
import os

class CalculatorWindow(qtw.QMainWindow):

    def __init__(self, *args, **kwargs ):
        super().__init__(*args, **kwargs)

        self.ui = Ui_gustav_beck_build_calculator()
        self.ui.setupUi(self)
        self.ui.actionCalculation.triggered.connect(self.open_pdf)

        self.ui.btn_calculate.clicked.connect(self.calculate)
    def calculate(self):
        serial_text=self.ui.txt_serial.text()
         
        try:
            serial=int(serial_text)
            #from 1850 - 1860
            if serial<=480 and serial<=3999:
                self.calculate_when_made(serial, 1850,10,480,3999)
            #from 1860-1863
            if serial>=4000 and serial<=9999:
                self.calculate_when_made(serial, 1860,3,4000,9999)
            #from 1863-1865
            if serial>=10000 and serial<=15000:
                self.calculate_when_made(serial, 1863,2,10000,15000)
            #from 1865-1867
            if serial>=15000 and serial<=24999:
                self.calculate_when_made(serial, 1865,2,15000,24999)
            #from 1867-1872
            if serial>=25000 and serial<=49999:
                self.calculate_when_made(serial, 1867,5,25000,49999)
            #from 1872-1875
            if serial>=50000 and serial<= 99999:
                self.calculate_when_made(serial, 1872,3,50000,99999)
            #from 1875-1880
            if serial>=100000 and serial<= 259999:
                self.calculate_when_made(serial, 1875,5,100000,259999)
            #from 1880-1885
            if serial>=260000 and serial<=499999:
                self.calculate_when_made(serial, 1880,5,260000,499999)
            #from 1885-1890
            if serial>=500000 and serial<=799999:
                self.calculate_when_made(serial, 1885,5,500000,799999)
            #from 1890-1892
            if serial>=800000 and serial<=999999:
                self.calculate_when_made(serial, 1890,2,800000,799999)
            #from 1892-1900
            if serial>=1000000 and serial<=1499999:
                self.calculate_when_made(serial, 1892,8,1000000,1499999)
            #from 1900-1913
            if serial>=1500000 and serial<=1849999:
                self.calculate_when_made(serial, 1900,13,1500000,1849999)
            #from 1913-1923
            if serial>=1850000 and serial<1859999:
                self.calculate_when_made(serial, 1913,10,1850000,1859999)
            #from 1923-1925
            if serial>=1860000 and serial<=1945398:
                self.calculate_when_made(serial, 1923,2,1860000,1945398)
            #from 1925-1926
            if serial>=1945399 and serial<=2244867:
                self.calculate_when_made(serial, 1925,1,1945399,2244867)
            #since 1926
            if serial>=2244868:
                self.ui.lbl_result.setText("Deze klok is van 1926, de maand kon nog niet berekend worden in deze versie van het programma.")
        except:
            self.ui.lbl_result.setText("Vul enkel nummers in bij het serie nummer vakje.")

    def calculate_when_made(self, serial,min_year,time_frame,min_clocks,max_clocks):
        months = ["Januari", "Februari", "Maart","April","Mei","Juni","Juli","Augustus","September","Oktober","November","December"]
        min_year=min_year
        time_period=time_frame
        clocks_made_each_year=(max_clocks - min_clocks)/time_period
        #calculates years passed since the clock had been made
        years_and_months_passed_since_clock_made=str(round(((serial-min_clocks) / clocks_made_each_year),1))
        #split up the year & month returned (
        # ex: 11.5 is made into 2 arrays, first one for year number
        # second for month index (this would make year xx11, and month june)
        year, month = map(int,years_and_months_passed_since_clock_made.split(".", 1))
        year,month = [year],[month]
        #add up the first value retrieved from above to the minimal year
        year_made=min_year+year[0]
        #the months array takes what ever the second value was of the above calculation, but it is also in a list so it is typed like so: str(months[month[0]])
        self.ui.lbl_result.setText("Deze klok is gemaakt rond "+str(months[month[0]])+" " + str(year_made))

    def open_pdf(self):
        print('opening pdf')
        __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        f = os.path.join(__location__, 'Berekening van vervaardiging Gustav Beck2.pdf')
        webbrowser.open_new(f)

if __name__ == "__main__":
    app = qtw.QApplication([])
    widget=CalculatorWindow()
    widget.show()
    app.exec_()