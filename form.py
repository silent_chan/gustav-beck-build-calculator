# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'form.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_gustav_beck_build_calculator(object):
    def setupUi(self, gustav_beck_build_calculator):
        gustav_beck_build_calculator.setObjectName("Gustav Beck Bouwjaar Calculator")
        gustav_beck_build_calculator.resize(504, 141)
        self.centralwidget = QtWidgets.QWidget(gustav_beck_build_calculator)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.lbl_serial = QtWidgets.QLabel(self.centralwidget)
        self.lbl_serial.setObjectName("lbl_serial")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.lbl_serial)
        self.txt_serial = QtWidgets.QLineEdit(self.centralwidget)
        self.txt_serial.setInputMethodHints(QtCore.Qt.ImhDigitsOnly|QtCore.Qt.ImhNoAutoUppercase|QtCore.Qt.ImhPreferNumbers)
        self.txt_serial.setObjectName("txt_serial")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.txt_serial)
        self.btn_calculate = QtWidgets.QPushButton(self.centralwidget)
        self.btn_calculate.setCheckable(False)
        self.btn_calculate.setAutoDefault(True)
        self.btn_calculate.setDefault(False)
        self.btn_calculate.setFlat(False)
        self.btn_calculate.setObjectName("btn_calculate")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.SpanningRole, self.btn_calculate)
        self.lbl_result = QtWidgets.QLabel(self.centralwidget)
        self.lbl_result.setText("")
        self.lbl_result.setObjectName("lbl_result")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.SpanningRole, self.lbl_result)
        self.verticalLayout.addLayout(self.formLayout)
        gustav_beck_build_calculator.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(gustav_beck_build_calculator)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 504, 24))
        self.menubar.setObjectName("menubar")
        self.menuHelp = QtWidgets.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        gustav_beck_build_calculator.setMenuBar(self.menubar)
        self.actionCalculation = QtWidgets.QAction(gustav_beck_build_calculator)
        self.actionCalculation.setObjectName("actionCalculation")
        self.menuHelp.addAction(self.actionCalculation)
        self.menubar.addAction(self.menuHelp.menuAction())

        self.retranslateUi(gustav_beck_build_calculator)
        QtCore.QMetaObject.connectSlotsByName(gustav_beck_build_calculator)

    def retranslateUi(self, gustav_beck_build_calculator):
        _translate = QtCore.QCoreApplication.translate
        gustav_beck_build_calculator.setWindowTitle(_translate("Gustav Beck Build Year Calculator", "Gustav Beck Bouwjaar Calculator"))
        self.lbl_serial.setText(_translate("Serial Number: ", "Serie Nummer:"))
        self.btn_calculate.setText(_translate("Calculate build year", "Bereken Bouwjaar"))
        self.menuHelp.setTitle(_translate("Help", "Help"))
        self.actionCalculation.setText(_translate("How is the calculation done", "Hoe is de berekening gedaan (PDF)"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    gustav_beck_build_calculator = QtWidgets.QMainWindow()
    ui = Ui_gustav_beck_build_calculator()
    ui.setupUi(gustav_beck_build_calculator)
    gustav_beck_build_calculator.show()
    sys.exit(app.exec_())
